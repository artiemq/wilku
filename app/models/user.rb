class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :trackable, :validatable, :authentication_keys => [:username]

  has_many :memberships, dependent: :destroy
  has_many :projects, through: :memberships

  has_many :projects, dependent: :destroy

  validates_presence_of :username
  validates_uniqueness_of :username

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def self.search(search)
    if search
      where(['lower(name) LIKE ? OR lower(username) LIKE ? OR lower(about) LIKE ? OR lower(stack) LIKE ?', "%#{search.downcase}%","%#{search.downcase}%","%#{search.downcase}%","%#{search.downcase}%"])
    else
      all
    end
  end

end
