class Project < ApplicationRecord
  belongs_to :user

  has_many :memberships, dependent: :destroy
  has_many :users, through: :memberships

  def self.search(search)
    if search
      where(['lower(name) LIKE ? OR lower(description) LIKE ?', "%#{search.downcase}%","%#{search.downcase}%"])
    else
      all
    end
  end
end
