class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end

  def index
    @users = User.order(message_count: :desc).all
    @projects = Project.all
  end
end
