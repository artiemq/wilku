class SearchController < ApplicationController
    def index
        @projects = Project.search(params[:search])
        @users = User.search(params[:search])
    end
end
