class HomeController < ApplicationController
  def index
    @users = User.order(message_count: :desc).all
    @projects = Project.all
  end
end
