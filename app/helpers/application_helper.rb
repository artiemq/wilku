module ApplicationHelper
    def nav_link(text, path)
    options = current_page?(path) ? { class: "active pink item" } : { class: "item" }
    content_tag(:li, options) do
      link_to text, path
    end
  end
end
