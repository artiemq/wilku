require 'net/http'
require 'json'
module UsersHelper

  @@api_link = "http://progconfbot.herokuapp.com/api/users/name/%s"
  @@info_link = "http://progconfbot.herokuapp.com/api/stat/%s/progConf"
  def get_first_info(username)
    link = @@api_link % [username]
    uri = URI(link)
    response = Net::HTTP.get(uri)
    data = JSON.parse(response)
    if data.present?
      { name:data["firstName"].to_s + (data["lastName"].present? ? " " + data["lastName"].to_s() :  ""),
       id: data["id"] }
    else
      nil
    end
  end

  def get_info(telegram_id)
    link = @@info_link % [telegram_id]
    uri = URI(link)
    response = Net::HTTP.get(uri)
    data = JSON.parse(response)
    if data.present?
      { name:data["user"]["firstName"].to_s + (data["user"]["lastName"].present? ? " " + data["user"]["lastName"].to_s() :  ""),
       message_count: data['messages'] }
    else
      nil
    end
  end
  
end
