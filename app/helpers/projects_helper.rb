module ProjectsHelper
  def user_in_project(project,user)
    for membership in project.memberships do
      if membership.user == user
        puts user.name
        return true
      end
    end
    false
  end

  def need_menu(project)
    not (user_in_project(project,current_user) and current_user != project.user and project.link.nil?)
  end
  
end
