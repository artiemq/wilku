require 'net/http'
require 'json'

require "#{Rails.root}/app/helpers/users_helper"
include UsersHelper


namespace :users do


  desc "Update users profile information"
  task update: :environment do

  for user in User.all do
    data = get_info(user.id)
    if data.present?
      puts data
      user.update(data)
    end
  end


  end

end
