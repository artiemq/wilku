class AddStackColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :stack, :text
  end
end
