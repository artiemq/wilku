class RemoveTelegramColumnsFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :telegram, :string
  end
end
