class AddColumnMessageCountToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :message_count, :int
  end
end
