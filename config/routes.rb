Rails.application.routes.draw do
  resources :projects, except: [:show]
  devise_for :users, controllers: { registrations: "users/registrations" }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :users, only: [:show]

  resources :search, only: [:index]

  root 'home#index'


end
